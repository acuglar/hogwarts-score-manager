import { useState } from "react";

import {
  CardContainer,
  Card,
  CharCard,
  CharCardFlagMini,
  CharCardImage,
  FlagMini,
} from "./styles";

const CardStudents = ({ char, students, isCard, handleIsCard }) => {
  const [isScore, setIsScore] = useState(false);
  const [score, setScore] = useState(0);

  console.log(students);

  const handleGain = (studentScore) => {
    setScore(score + 10);
    studentScore = score;
    setIsScore(!isScore);
  };

  const handleLose = (studentScore) => {
    setScore(score - 10);
    studentScore = score;
    setIsScore(!isScore);
  };

  return (
    <CardContainer>
      <Card>
        {students
          .filter((student) => student.name === char)
          .map((student) => (
            <CharCard>
              <div>
                <CharCardImage src={student.image} alt={student.name} />
              </div>
              <div>
                <CharCardFlagMini>
                  <FlagMini src={student.flagMini} alt={student.house} />
                  <h3>{student.house}</h3>
                </CharCardFlagMini>
                <div>
                  <h1>{student.name}</h1>
                </div>
                {!isScore ? (
                  <div>
                    <button
                      onClick={() => handleGain(console.log(student.score))}
                    >
                      Gain
                    </button>
                    <button onClick={() => handleLose(student.score)}>
                      Lose
                    </button>
                  </div>
                ) : (
                  <div disabled={isCard}>
                    <h2>{score > 0 ? `+${score}` : score}</h2>
                    <button onClick={handleIsCard}>Done</button>
                  </div>
                )}
              </div>
            </CharCard>
          ))}
      </Card>
    </CardContainer>
  );
};

export default CardStudents;
