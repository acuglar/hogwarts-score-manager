import styled from 'styled-components'


export const CardContainer = styled.div`
position: absolute;
top: 0vh;
left: 0;
width: 100%;
height: 200vh;
background-color: rgba(0,0,0,.5);
z-index: 2;
`;


export const Card = styled.div`
color: black;
position: absolute;
top: -25%;
left: 50%;
transform: translate(-50%, -50%);
z-index: 3;
background-color: lightgray;
`;

export const CharCard = styled.div`
  display: flex;
  border: 1px solid red;
`;

export const CharCardFlagMini = styled.div`
  display: flex;
`;

export const CharCardImage = styled.img`
  width: 162px;
  height: 228px;
`;

export const FlagMini = styled.img`
  width: 60px;
`;