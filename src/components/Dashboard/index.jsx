import { useState, useEffect } from "react";
import axios from "axios";

import React from "react";
import DashboardStudents from "../DashboardStudents";
import DashboardScore from "../DashboardScore";

const Dashboard = () => {
  const [students, setStudents] = useState([]);
  const [url, setUrl] = useState(
    `https://hp-api.herokuapp.com/api/characters/students`
  );

  const request = () => {
    axios
      .get(url)
      .then((response) => {
        setStudents([...response.data]);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    request();
  }, []);

  const addProps = (element) => {
    element.score = 0;
    // eslint-disable-next-line default-case
    switch (element.house) {
      case "Gryffindor":
        return (
          (element.flag = "/images/gryffindor.png"),
          (element.flagMini = "/images/gryffindor-mini.png")
        );
      case "Hufflepuff":
        return (
          (element.flag = "/images/hufflepuff.png"),
          (element.flagMini = "/images/hufflepuff-mini.png")
        );
      case "Ravenclaw":
        return (
          (element.flag = "/images/ravenclaw.png"),
          (element.flagMini = "/images/ravenclaw-mini.png")
        );
      case "Slytherin":
        return (
          (element.flag = "/images/slytherin.png"),
          (element.flagMini = "/images/slytherin-mini.png")
        );
    }
  };
  const studentsExtended = students;
  studentsExtended.forEach((student) => addProps(student));
  console.log(studentsExtended);
  console.log(students);

  return (
    <>
      <DashboardScore students={students} />
      <DashboardStudents students={students} />
    </>
  );
};

export default Dashboard;
