import { ScoreContainer, ScoreFlag, ScoreImg, ScoreInner } from "./styles";

const DashboardScore = ({ students }) => {
  const reducer = (house, array) => {
    return array
      .filter((student) => student.house === house)
      .reduce((acc, student) => acc + student.score, 0);
  };

  return (
    <ScoreContainer>
      <ScoreFlag>
        {students
          .filter(
            (student, index, array) =>
              array.findIndex((t) => t.house === student.house) === index
          )
          .map((student, index) => (
            <ScoreInner key={index}>
              <div>{`#${index + 1} ${student.house}`}</div>
              <ScoreImg src={student.flag} alt={student.name} />
              <div>{reducer(student.house, students)}</div>
            </ScoreInner>
          ))
          .sort((a, b) => b.score - a.score)}
      </ScoreFlag>
    </ScoreContainer>
  );
};

export default DashboardScore;
