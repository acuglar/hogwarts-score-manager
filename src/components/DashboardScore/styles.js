import styled from 'styled-components'

export const ScoreContainer = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  top: 18vh;
  padding: 0 200px;
  z-index: 1;
`;

export const ScoreFlag = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
`;

export const ScoreInner = styled.div`
  font-size: x-large;
  color: lightgray;
`;

export const ScoreImg = styled.img`
  border: 8px outset lightgray;
`;
