import { TableContainer, TableOuter } from "./styles";

import { useState } from "react";
import { RiFilePaper2Line } from "react-icons/ri";

import CardStudents from "../CardStudents";

const DashBoardStudents = ({ students }) => {
  const [isCard, setIsCard] = useState(false);
  const [char, setChar] = useState("");

  const handleIsCard = (char) => {
    setIsCard(!isCard);
    setChar(char);
  };

  return (
    <TableContainer style={{ bgColor: "#333333" }}>
      <TableOuter>
        <div>
          <h1>Students</h1>
        </div>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>House</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {students.map((student, index) => {
              return (
                <tr key={index}>
                  <td>{student.name}</td>
                  <td>{student.house}</td>
                  <td>
                    <RiFilePaper2Line
                      onClick={() => handleIsCard(student.name)}
                      isCard={isCard}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {isCard && (
          <CardStudents
            handleIsCard={handleIsCard}
            isCard={isCard}
            char={char}
            students={students}
          />
        )}
      </TableOuter>
    </TableContainer>
  );
};

export default DashBoardStudents;
