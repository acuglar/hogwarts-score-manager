import styled from 'styled-components'

export const TableContainer = styled.div`
display: flex;
justify-content: center;
font-size: 20px;
color: lightgray;
position: absolute;
top: 70vh;
padding: 0 4%;
width: 92%;
`;

export const TableOuter = styled.div`
display: flex;
flex-direction: column;
width: 80%;
background-color: #515151;
border-radius: 15px;
box-shadow: #000 2px 2px 1px;

  h1 {
    text-align: left;
    margin-left: 20px;
  }

  table {
    line-height: 2rem;
    font-variant: small-caps;
    margin: 20px;
    background-color: #424242;
  }

  thead {
    font-size: xx-large;
  }

  tbody {
    font-size: x-large;
  }
  
  td {
    border-bottom: 1px solid lightgray;
    
  }

  tr {
    margin: 10px;
  }
`;

