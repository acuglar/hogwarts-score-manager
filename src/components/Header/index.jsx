import { HeaderOuter, HeaderInner } from "./styles";

const Header = () => {
  return (
    <HeaderOuter>
      <HeaderInner>
        <img src="/images/houses.png" alt="houses" />
        <div>Hogwarts Score Maneger</div>
      </HeaderInner>
    </HeaderOuter>
  );
};

export default Header;
