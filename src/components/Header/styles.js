import styled from 'styled-components'

export const HeaderOuter = styled.div`
position: absolute;
font-size: 24px;
background-color: #333;
height: 22vh;
width: 100%;
left: 0px;
top: 0px;
box-shadow: 0 0 1em gold;
`;

export const HeaderInner = styled.div`
  display: flex;
  position: relative;
  background-color: gold;
  border-top: 8px solid yellow; 
  border-bottom: 8px solid yellow; 
  height: 10vh;
  line-height: 10vh;
  width: 95%;
  top: 20px;
  text-align: left;
  padding-left: 5%;

  img {
    margin-right: 5%;
  }
`;
