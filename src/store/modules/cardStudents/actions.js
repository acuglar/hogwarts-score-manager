import { ADD_NUMBER, SUB_NUMBER } from './actionsTypes'

export const addNumber = (number) => ({
  type: ADD_NUMBER,
  number
});

export const subNumber = (number) => ({
  type: SUB_NUMBER,
  number
})
